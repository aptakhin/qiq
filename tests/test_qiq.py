from qiq import Dependency, Executor


def test_dependency():
	D = Dependency
	DT = Dependency.Type
	assert D(DT.PYPI, set(), "liba", "liba") == D.parse("liba")
	assert D(DT.PYPI, {"dev"}, "liba", "liba") == D.parse("+dev:liba")
	assert D(DT.LOCAL, set(), "libc", "../libc") == D.parse("../libc")
	assert D(DT.LOCAL, {"dev"}, "libc", "../libc") == D.parse("+dev:../libc")
	assert D(DT.LOCAL, set(), "libc", "../libc-dev") == D.parse("libc==../libc-dev")
	assert D(DT.LOCAL, {"dev"}, "libc", "../libc-dev") == D.parse("+dev:libc==../libc-dev")

	assert (D(DT.VCS, set(), "myproj", "git+ssh://gitlab.com/myteam/myproject.git@2313ceee") ==
	        D.parse("myproj==git+ssh://gitlab.com/myteam/myproject.git@2313ceee"))


def test_rewrite():
	exec = Executor()
	dep = Dependency(dtype=Dependency.Type.PYPI, condition=set(), name="liba", path="")
	good_resp = "liba==0.13\nlibb"
	assert good_resp == exec._rewrite_version_content("liba\nlibb", dep, "0.13")
	# Apply one again to new content. Must be no diff
	assert good_resp == exec._rewrite_version_content(good_resp, dep, "0.13")
	# Update version
	assert "liba==0.14\nlibb" == exec._rewrite_version_content(good_resp, dep, "0.14")

	assert "curliba\nliba==0.13\ncurliba-wide" == exec._rewrite_version_content("curliba\nliba\ncurliba-wide", dep, "0.13")
	assert "liba==0.13\nliba-wide" == exec._rewrite_version_content("liba\nliba-wide", dep, "0.13")
	assert "lib0\nliba==0.13\nliba-wide" == exec._rewrite_version_content("lib0\nliba\nliba-wide", dep, "0.13")


if __name__ == "__main__":
	test_dependency()
	test_rewrite()
